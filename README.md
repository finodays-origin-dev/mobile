# Finodays

[![License: MIT][license_badge]][license_link]

Приложение для распознования маски и эмоций у посетителей банка
---

## Getting Started 🚀

Чтобы запустить:

```sh
$ flutter run
```

Чтобы собрать под андроид:

```sh
$ flutter build apk --split-per-abi
```

*Finodays works on iOS, Android*

---
[license_badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license_link]: https://opensource.org/licenses/MIT
