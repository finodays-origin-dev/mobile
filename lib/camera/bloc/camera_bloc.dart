import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:camera/camera.dart';
import 'package:finodays/camera/domain/recognition_response.dart';
import 'package:finodays/camera/domain/recognitions_api.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';
import 'package:image/image.dart' as imagelib;

part 'camera_event.dart';
part 'camera_state.dart';
part 'camera_bloc.freezed.dart';

class CameraBloc extends Bloc<CameraEvent, CameraState> {
  CameraBloc(this._api) : super(const CameraState.initial()) {
    on<_SendImage>(_onSendImage, transformer: droppable());
  }

  final Queue<Emotions> _lastEmotions = Queue();

  Queue<Emotions> get lastEmotions => _lastEmotions;

  final RecognitionApi _api;

  Future<void> _onSendImage(_SendImage event, Emitter<CameraState> emit) async {
    if (state is _Initial) {
      emit(const _Loaded(loading: true, maskStatus: MaskStatus.none));
    } else {
      emit((state as _Loaded).copyWith(loading: true));
    }
    final value = await compute<ImagePayload, imagelib.Image>(
      _takePicture,
      ImagePayload(event.faces, event.original, event.inputImage.inputImageData!),
    );

    final response = await _api.recognize(
      MultipartFile.fromBytes(
        'upload.png',
        encodePng(value),
        filename: 'upload.png',
        contentType: MediaType('image', 'jpeg'),
      ),
    );
    if (response.isSuccessful) {
      final json = jsonDecode(response.body as String) as Map<String, dynamic>;
      final result = RecognitionResponse.fromJson(json);
      if (result.emotion != null) {
        _lastEmotions.addFirst(result.emotion!);
        if (_lastEmotions.length > 3) {
          _lastEmotions.removeLast();
        }
      }
      final emotion = _lastEmotions.pickMostCommon();
      // var emotion = Emotions.values[_index];
      // _index += 1;
      // if (_index == Emotions.values.length) {
      //   _index = 0;
      // }

      emit((state as _Loaded).copyWith(emotion: emotion, maskStatus: result.mask, loading: false));
    } else {
      // var emotion = Emotions.values[_index];
      // _index += 1;
      // if (_index == Emotions.values.length) {
      //   _index = 0;
      // }
      emit(
        (state as _Loaded).copyWith(maskStatus: MaskStatus.none, loading: false, emotion: Emotions.fear),
      );
    }
  }
}

extension PickMost<T> on Queue<T> {
  T pickMostCommon() {
    final items = toList();
    final popular = <T, int>{};

    for (final el in items) {
      popular[el] = (popular[el] ?? 0) + 1;
    }

    final sortedValues = popular.keys.toList()..sort((a, b) => popular[b]!.compareTo(popular[a]!));

    return sortedValues.first;
  }
}

class ImagePayload {
  ImagePayload(this.faces, this.original, this.inputImageData);

  final List<Face> faces;
  final CameraImage original;
  final InputImageData inputImageData;
}

Future<imagelib.Image> _takePicture(ImagePayload payload) {
  final faces = payload.faces;
  final original = payload.original;
  final image = Platform.isAndroid ? convertYUV420(original) : _convertBGRA8888(original);
  final face = faces[0];
  final x = face.boundingBox.left.toInt();
  final y = face.boundingBox.top.toInt();
  final w = face.boundingBox.right.toInt() - x;
  final h = face.boundingBox.bottom.toInt() - y;
  final rotated = imagelib.copyRotate(image, Platform.isAndroid ? payload.inputImageData.imageRotation.rawValue : 0);
  final croppedImage = imagelib.copyCrop(rotated, x, y, w, h);
  final result = imagelib.copyResize(croppedImage, width: 224, height: 224);

  return Future.value(result);
}

// CameraImage YUV420_888 -> PNG -> Image (compresion:0, filter: none)
// Black

imagelib.Image _convertBGRA8888(CameraImage image) {
  return imagelib.Image.fromBytes(
    image.width,
    image.height,
    image.planes[0].bytes,
    format: imagelib.Format.bgra,
  );
}

imagelib.Image convertYUV420(CameraImage image) {
  const shift = 0xFF << 24;

  final width = image.width;
  final height = image.height;
  final uvRowStride = image.planes[1].bytesPerRow;
  final uvPixelStride = image.planes[1].bytesPerPixel!;

  // imgLib -> Image package from https://pub.dartlang.org/packages/image
  var img = imagelib.Image(width, height);

  // Fill image buffer with plane[0] from YUV420_888
  for (var x = 0; x < width; x++) {
    for (var y = 0; y < height; y++) {
      final uvIndex = uvPixelStride * (x / 2).floor() + uvRowStride * (y / 2).floor();
      final index = y * width + x;

      final yp = image.planes[0].bytes[index];
      final up = image.planes[1].bytes[uvIndex];
      final vp = image.planes[2].bytes[uvIndex];
      // Calculate pixel color
      var r = (yp + vp * 1436 / 1024 - 179).round().clamp(0, 255);
      var g = (yp - up * 46549 / 131072 + 44 - vp * 93604 / 131072 + 91).round().clamp(0, 255);
      var b = (yp + up * 1814 / 1024 - 227).round().clamp(0, 255);
      // color: 0x FF  FF  FF  FF
      //           A   B   G   R
      img.data[index] = shift | (b << 16) | (g << 8) | r;
    }
  }

  return img;
}
