part of 'camera_bloc.dart';

@freezed
class CameraEvent with _$CameraEvent {
  const factory CameraEvent.started() = _Started;
  const factory CameraEvent.sendImage({required InputImage inputImage, required List<Face> faces, required CameraImage original}) = _SendImage;
}
