part of 'camera_bloc.dart';

@freezed
class CameraState with _$CameraState {
  const factory CameraState.initial() = _Initial;
  const factory CameraState.loaded({
    required bool loading,
    required MaskStatus maskStatus,
    Emotions? emotion,
  }) = _Loaded;
}

enum Emotions {
  anger('https://assets10.lottiefiles.com/packages/lf20_syxn7sbq.json', 'Злость', 60),
  disgust('https://assets4.lottiefiles.com/packages/lf20_htnyf43v.json', 'Недовольство', 80),
  fear('https://assets8.lottiefiles.com/private_files/lf30_qolzpdwh.json', 'Страх', 65),
  happiness('https://assets9.lottiefiles.com/packages/lf20_slimwle6.json', 'Счастье', 85),
  sadness('https://assets10.lottiefiles.com/packages/lf20_pojzngga.json', 'Грусть', 65),
  surprise('https://assets5.lottiefiles.com/packages/lf20_coohneqq.json', 'Удивление', 90),
  neutral('https://assets8.lottiefiles.com/packages/lf20_hntzagxx.json', 'Нейтральное', 70),
  contempt('https://assets4.lottiefiles.com/packages/lf20_htnyf43v.json', 'Презрение', 80);

  const Emotions(this.url, this.text, this.size);
  final String url;
  final String text;
  final int size;
}

enum MaskStatus {
  none('Нет маски'),
  full('Есть маска'),
  wrong('Амогус');

  const MaskStatus(this.text);
  final String text;
}
