import 'package:finodays/camera/camera.dart';
import 'package:json_annotation/json_annotation.dart';

part 'recognition_response.g.dart';

@JsonSerializable()
class RecognitionResponse {
  RecognitionResponse({this.emotion, required this.mask});

  factory RecognitionResponse.fromJson(Map<String, dynamic> json) => _$RecognitionResponseFromJson(json);

  final Emotions? emotion;
  final MaskStatus mask;
}
