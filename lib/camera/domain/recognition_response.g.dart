// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recognition_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecognitionResponse _$RecognitionResponseFromJson(Map<String, dynamic> json) =>
    RecognitionResponse(
      emotion: $enumDecodeNullable(_$EmotionsEnumMap, json['emotion']),
      mask: $enumDecode(_$MaskStatusEnumMap, json['mask']),
    );

Map<String, dynamic> _$RecognitionResponseToJson(
        RecognitionResponse instance) =>
    <String, dynamic>{
      'emotion': _$EmotionsEnumMap[instance.emotion],
      'mask': _$MaskStatusEnumMap[instance.mask]!,
    };

const _$EmotionsEnumMap = {
  Emotions.anger: 'anger',
  Emotions.disgust: 'disgust',
  Emotions.fear: 'fear',
  Emotions.happiness: 'happiness',
  Emotions.sadness: 'sadness',
  Emotions.surprise: 'surprise',
  Emotions.neutral: 'neutral',
  Emotions.contempt: 'contempt',
};

const _$MaskStatusEnumMap = {
  MaskStatus.none: 'none',
  MaskStatus.full: 'full',
  MaskStatus.wrong: 'wrong',
};
