// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recognitions_api.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations, unnecessary_brace_in_string_interps
class _$RecognitionApi extends RecognitionApi {
  _$RecognitionApi([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = RecognitionApi;

  @override
  Future<Response<dynamic>> recognize(http.MultipartFile image) {
    final String $url = 'upload';
    final List<PartValue> $parts = <PartValue>[
      PartValueFile<http.MultipartFile>(
        'upload.png',
        image,
      )
    ];
    final Request $request = Request(
      'POST',
      $url,
      client.baseUrl,
      parts: $parts,
      multipart: true,
    );
    return client.send<dynamic, dynamic>($request);
  }
}
