import 'package:chopper/chopper.dart';
import 'package:http/http.dart' as http;

part 'recognitions_api.chopper.dart';

@ChopperApi()
abstract class RecognitionApi extends ChopperService {
  static RecognitionApi create([ChopperClient? client]) => _$RecognitionApi(client);

  @Post(path: 'upload')
  @multipart
  Future<Response<dynamic>> recognize(
    @PartFile('upload.png') http.MultipartFile image,
  );
}
