import 'package:finodays/camera/camera.dart';
import 'package:finodays/camera/view/face_detector.dart';
import 'package:finodays/di/di_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CameraPage extends StatelessWidget {
  const CameraPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CameraBloc(getIt.getApi()),
      child: const FaceDetectorView(),
    );
  }
}
