import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:finodays/main.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_mlkit_commons/google_mlkit_commons.dart';

enum ScreenMode { liveFeed, gallery }

class CameraView extends StatefulWidget {
  const CameraView({
    super.key,
    required this.customPaint,
    required this.onImage,
    this.onScreenModeChanged,
    this.initialDirection = CameraLensDirection.back,
  });

  final ValueNotifier<CustomPaint?> customPaint;
  final void Function(InputImage? inputImage, CameraImage? original) onImage;
  final void Function(ScreenMode mode)? onScreenModeChanged;
  final CameraLensDirection initialDirection;

  @override
  State<CameraView> createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> with WidgetsBindingObserver {
  CameraController? _controller;
  int _cameraIndex = -1;
  double _baseScaleFactor = 1, minZoomLevel = 0, maxZoomLevel = 0, _scaleFactor = 1;
  bool _changingCameraLens = false;

  @override
  void initState() {
    super.initState();

    _cameraIndex = 1;

    _startLiveFeed();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _stopLiveFeed();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _liveFeedBody(),
      floatingActionButton: _floatingActionButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  Widget? _floatingActionButton() {
    if (cameras.length == 1) return null;
    return SizedBox(
      height: 70,
      width: 70,
      child: FloatingActionButton(
        elevation: 0,
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.white,
        onPressed: _switchLiveCamera,
        child: Icon(
          Platform.isIOS ? Icons.flip_camera_ios_outlined : Icons.flip_camera_android_outlined,
          size: 40,
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // if (_controller == null || !_controller!.value.isInitialized) {
    //   return;
    // }
    if (state == AppLifecycleState.inactive) {
      _stopLiveFeed();
    } else if (state == AppLifecycleState.resumed) {
      _startLiveFeed();
    }
  }

  Widget _liveFeedBody() {
    if (_controller == null || !_controller!.value.isInitialized) {
      return Container();
    }

    final size = MediaQuery.of(context).size;
    var scale = size.aspectRatio * _controller!.value.aspectRatio * 0.97;

    if (scale < 1) scale = 1 / scale;

    return ColoredBox(
      color: Colors.black,
      child: Stack(
        fit: StackFit.expand,
        alignment: Alignment.center,
        children: <Widget>[
          GestureDetector(
            onScaleStart: (_) {
              _baseScaleFactor = _scaleFactor;
            },
            onScaleUpdate: (details) {
              _scaleFactor = _baseScaleFactor * details.scale;

              _controller?.setZoomLevel(clampDouble(_scaleFactor, minZoomLevel, maxZoomLevel));
            },
            child: Transform.scale(
              scale: scale,
              child: Center(
                child: CameraPreview(_controller!),
              ),
            ),
          ),
          ValueListenableBuilder(
              valueListenable: widget.customPaint,
              builder: (context, value, child) {
                if (value != null) {
                  return value;
                }
                return SizedBox();
              })
        ],
      ),
    );
  }

  Future<void> _startLiveFeed() async {
    final camera = cameras[_cameraIndex];
    _controller = CameraController(
      camera,
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid ? ImageFormatGroup.yuv420 : ImageFormatGroup.bgra8888,
    );
    unawaited(
      _controller?.initialize().then((_) {
        if (!mounted) {
          return;
        }
        if (!mounted) {
          return;
        }
        _controller?.getMinZoomLevel().then((value) {
          minZoomLevel = value;
        });
        _controller?.getMaxZoomLevel().then((value) {
          maxZoomLevel = value;
        });
        if (!kIsWeb) {
          _controller?.startImageStream(_processCameraImage);
        } else {}
        setState(() {});
      }),
    );
  }

  Future<void> _stopLiveFeed() async {
    _controller?.stopImageStream();
    _controller?.dispose();
    _controller = null;
    setState(() {});
  }

  Future<void> _switchLiveCamera() async {
    setState(() => _changingCameraLens = true);
    await _stopLiveFeed();
    _cameraIndex = (_cameraIndex + 1) % cameras.length;

    await _startLiveFeed();
    setState(() => _changingCameraLens = false);
  }

  Future<void> _processCameraImage(CameraImage image) async {
    final allBytes = WriteBuffer();
    for (final plane in image.planes) {
      allBytes.putUint8List(plane.bytes);
    }

    final bytes = allBytes.done().buffer.asUint8List();

    final imageSize = Size(image.width.toDouble(), image.height.toDouble());

    final camera = cameras[_cameraIndex];
    final imageRotation = InputImageRotationValue.fromRawValue(camera.sensorOrientation);
    if (imageRotation == null) return;

    final inputImageFormat = Platform.isAndroid ? InputImageFormat.yuv420 : InputImageFormat.bgra8888;

    final planeData = image.planes.map(
      (Plane plane) {
        return InputImagePlaneMetadata(
          bytesPerRow: plane.bytesPerRow,
          height: plane.height,
          width: plane.width,
        );
      },
    ).toList();

    final inputImageData = InputImageData(
      size: imageSize,
      imageRotation: imageRotation,
      inputImageFormat: inputImageFormat,
      planeData: planeData,
    );

    final inputImage = InputImage.fromBytes(bytes: bytes, inputImageData: inputImageData);

    widget.onImage(inputImage, image);
  }
}
