import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:finodays/camera/camera.dart';
import 'package:finodays/camera/view/custom_painter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:image/image.dart' as im;

import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

class FaceDetectorView extends StatefulWidget {
  const FaceDetectorView({super.key});

  @override
  State<FaceDetectorView> createState() => _FaceDetectorViewState();
}

class _FaceDetectorViewState extends State<FaceDetectorView> with TickerProviderStateMixin {
  FaceDetector? _faceDetector;
  bool _canProcess = true;
  bool _isBusy = false;
  CustomPaint? _customPaint;

  Image? _image;

  late AnimationController _controller;

  late Animation<Color?> animationOne;
  late Animation<Color?> animationTwo;

  static const beginColor = Colors.white30;
  static const endColor = Colors.white;

  final _format = DateFormat('dd MMM HH:mm:ss', 'ru_RU');

  final ValueNotifier<CustomPaint?> _paintValue = ValueNotifier<CustomPaint?>(null);

  @override
  void initState() {
    super.initState();
    if (!kIsWeb) {
      _faceDetector = FaceDetector(
        options: FaceDetectorOptions(),
      );
    }

    _controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 1500));

    animationOne = ColorTween(
      begin: beginColor,
      end: endColor,
    ).animate(_controller);

    animationTwo = ColorTween(
      begin: endColor,
      end: beginColor,
    ).animate(_controller);
  }

  @override
  void dispose() {
    _canProcess = false;
    _faceDetector?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CameraBloc, CameraState>(
        builder: (context, state) {
          final padding = state.maybeMap(orElse: () => null, loaded: (s) => s.emotion?.size) ?? 0;
          final hasImage = state.map(initial: (_) => false, loaded: (s) => true);
          return Stack(
            children: [
              SafeArea(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: FractionallySizedBox(
                    heightFactor: 0.8,
                    widthFactor: 1,
                    child: CameraView(
                      customPaint: _paintValue,
                      onImage: processImage,
                      initialDirection: CameraLensDirection.front,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0,
                right: 0,
                top: 30,
                child: SafeArea(
                  child: BlocBuilder<CameraBloc, CameraState>(
                    builder: (context, state) {
                      if (state.maybeMap(orElse: () => true, loaded: (s) => s.emotion == null)) {
                        return SizedBox();
                      }
                      var radius = 100.0;
                      var url =
                          state.maybeMap(orElse: () => null, loaded: (s) => 'assets/anim/${s.emotion?.name}.json');
                      if (state.map(initial: (_) => false, loaded: (s) => s.maskStatus == MaskStatus.full)) {
                        radius = 100;
                        url = 'assets/anim/mask.json';
                      }
                      if (url == null) {
                        return const SizedBox();
                      }

                      return Center(
                        child: DecoratedBox(
                          decoration: const BoxDecoration(shape: BoxShape.circle),
                          child: AnimatedSwitcher(
                            duration: const Duration(milliseconds: 200),
                            child: Lottie.asset(
                              url,
                              width: radius,
                              height: radius,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              if (hasImage)
                Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height * 0.2,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          BlocBuilder<CameraBloc, CameraState>(
                            builder: (context, state) {
                              return state.map(
                                initial: (_) => const SizedBox(),
                                loaded: (state) => Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        _DateMoodColumn(
                                          title: 'Дата',
                                          info: _format.format(DateTime.now()),
                                        ),
                                        _DateMoodColumn(
                                          title: 'Настроение',
                                          info: state.emotion?.text ?? 'Не известно',
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 16,
                                    ),
                                    const _TitleText(
                                      title: 'Средства индивидуальной защиты',
                                    ),
                                    Text(
                                      state.maskStatus.text,
                                      style: TextStyle(
                                        color: state.maskStatus == MaskStatus.none
                                            ? const Color(0xFFE33030)
                                            : Colors.green,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              if (_image != null) _image!
            ],
          );
        },
      ),
    );
  }

  Future<void> processImage(InputImage? inputImage, CameraImage? original) async {
    if (kIsWeb) {
      return;
    }
    if (!_canProcess) return;
    if (_isBusy) return;
    _isBusy = true;

    if (inputImage != null) {
      final bloc = context.read<CameraBloc>();
      final faces = await _faceDetector!.processImage(inputImage);

      if (inputImage.inputImageData?.size != null &&
          inputImage.inputImageData?.imageRotation != null &&
          faces.isNotEmpty) {
        // final painter =
        //     FaceDetectorPainter(faces, inputImage.inputImageData!.size, inputImage.inputImageData!.imageRotation);
        // _paintValue.value = null;

        bloc.add(CameraEvent.sendImage(inputImage: inputImage, original: original!, faces: faces));
      } else {
        _customPaint = null;
        _image = null;
      }
    } else {}
    _isBusy = false;
  }
}

class _DateMoodColumn extends StatelessWidget {
  const _DateMoodColumn({
    super.key,
    required this.title,
    required this.info,
  });

  final String title;
  final String info;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _TitleText(title: title),
        const SizedBox(
          height: 2,
        ),
        Text(
          info,
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

class _TitleText extends StatelessWidget {
  const _TitleText({
    super.key,
    required this.title,
  });
  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: Colors.white,
      ),
    );
  }
}
