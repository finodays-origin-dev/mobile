import 'dart:async';

import 'package:chopper/chopper.dart';

class AppClient {
  static ChopperClient createClient(List<ChopperService> services) {
    final client = ChopperClient(
      baseUrl: 'http://finodays.dchudinov.ru/api/',
      interceptors: [
        HttpLoggingInterceptor(),
        ModelConverter()
      ],
      services: services,
    );
    return client;
  }
}

// TODO: сделать авторизацию если надо
class AuthInterceptor implements RequestInterceptor {
  AuthInterceptor();

  @override
  FutureOr<Request> onRequest(Request request) {
    return request;
  }
}

class ModelConverter implements HeadersInterceptor {
  //
  @override
  Map<String, String> get headers => {contentTypeKey: 'multipart/form-data'};

  @override
  Future<Request> onRequest(Request request) async {
    final req = applyHeader(
      request,
      contentTypeKey,
      'multipart/form-data',
      override: true,
    );
    return req;
  }
}
