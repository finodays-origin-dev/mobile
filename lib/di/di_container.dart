import 'package:chopper/chopper.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.I;

extension ApiServiceLocator on GetIt {
  T getApi<T extends ChopperService>() => get<ChopperClient>().getService();
}
