import 'package:finodays/camera/domain/recognitions_api.dart';
import 'package:finodays/client/client.dart';
import 'package:finodays/di/di_container.dart';
import 'package:finodays/router/app_router.dart';

Future<void> initDi() async {
  getIt
    ..registerSingleton(AppClient.createClient([RecognitionApi.create()]))
    ..registerSingleton(AppRouter());
}
