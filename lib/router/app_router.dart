import 'package:auto_route/auto_route.dart';
import 'package:finodays/camera/view/camera_page.dart';
import 'package:flutter/material.dart';

part 'app_router.gr.dart';

@MaterialAutoRouter(routes: [AutoRoute(page: CameraPage, initial: true)])
class AppRouter extends _$AppRouter {}
