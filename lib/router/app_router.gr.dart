// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

part of 'app_router.dart';

class _$AppRouter extends RootStackRouter {
  _$AppRouter([GlobalKey<NavigatorState>? navigatorKey]) : super(navigatorKey);

  @override
  final Map<String, PageFactory> pagesMap = {
    CameraPageRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const CameraPage(),
      );
    }
  };

  @override
  List<RouteConfig> get routes => [
        RouteConfig(
          CameraPageRoute.name,
          path: '/',
        )
      ];
}

/// generated route for
/// [CameraPage]
class CameraPageRoute extends PageRouteInfo<void> {
  const CameraPageRoute()
      : super(
          CameraPageRoute.name,
          path: '/',
        );

  static const String name = 'CameraPageRoute';
}
