import 'package:finodays/theme/colors.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData get light => FlexThemeData.light(
        blendLevel: 12,
        surfaceMode: FlexSurfaceMode.highSurfaceLowScaffold,
        appBarElevation: 0.5,
        tabBarStyle: FlexTabBarStyle.forBackground,
        scheme: FlexScheme.mandyRed,
        appBarStyle: FlexAppBarStyle.surface,
        scaffoldBackground: Colors.black,
        // colors: FlexSchemeColor.from(
        primary: AppColors.primary,
        secondary: const Color(0xFFB9ED74),

        subThemesData: const FlexSubThemesData(
          inputDecoratorFillColor: Colors.white,
          inputDecoratorUnfocusedHasBorder: false,
          cardRadius: 16,
          elevatedButtonElevation: 0,
        ),
      ).copyWith(
        useMaterial3: true,
      );

  static ThemeData get dark => light;
}
